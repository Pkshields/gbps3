<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include '../script/base.php';
	
	//connect to database
	define('DBTest', TRUE);
	include '../script/db.php';
	
	//Get data from gb_feature
	$sql = "SELECT * FROM gb_tip ORDER BY ID ASC";
	$result = mysql_query($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <link href="files/style.css" rel="stylesheet" type="text/css" /> -->
<title>GBPS3 Admin - Tips Management</title>
</head>

<body>
  <h2>GBPS3 Admin - Tips Management</h2>
  <?PHP
	if (isset($_GET["error"])) {
  ?>
  <h4>Error: Required field(s) "<?PHP echo $_GET["error"]; ?>" are missing or incorrect, please try again.</h4>
  <?PHP
	}
  ?>
  <h3>Add Tip</h3>
  <form id="frmTip" name="frmTip" method="post" action="admin/script/tip.php">
    <ul>
	  <li>Tip: <input name="txtTip" type="text" id="txtTip" maxlength="200" /></li>
    </ul>
	<input type="submit" name="SubmitAdd" value="Add Tip" />
  </form>
  <h3>Delete Tip</h3>
  <form id="frmDelTip" name="frmDelTip" method="post" action="admin/script/tip.php">
    <ul>
      <li><select name="txtID" id="txtID">
	  <?PHP
		//Count number of categories in database
		$num=mysql_num_rows($result);
		
		//Print all Tips in list
		for ($i = 0; $i < $num; $i++) {
			echo '<option value="' . mysql_result($result,$i,"ID") . '" >' . mysql_result($result,$i,"Tip") . '</option>';
		}
		//Close Database
		mysql_close();
	  ?>
	  </select></li>
	</ul>
	<input type="submit" name="SubmitDel" value="Delete Tip" />
  </form>
  <p><a href="admin">Back to hub</a></p>
</body>
</html>