<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include '../script/base.php';
	
	//connect to database
	define('DBTest', TRUE);
	include '../script/db.php';
	
	//Get data from gb_feature
	$sql = "SELECT * FROM gb_user ORDER BY Username ASC";
	$result = mysql_query($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <link href="files/style.css" rel="stylesheet" type="text/css" /> -->
<title>GBPS3 Admin - User Management</title>
</head>

<body>
  <h2>GBPS3 Admin - User Management</h2>
  <?PHP
	if (isset($_GET["error"])) {
  ?>
  <h4>Error: Required field(s) "<?PHP echo $_GET["error"]; ?>" are missing or incorrect, please try again.</h4>
  <?PHP
	}
  ?>
  <h3>Add User</h3>
  <form id="frmUser" name="frmUser" method="post" action="admin/script/user.php">
    <ul>
	  <li>Username: <input name="txtUsername" type="text" id="txtUsername" maxlength="20" /></li>
	  <li>Password: <input name="txtPassword1" type="password" id="txtPassword1" maxlength="20" /></li>
	  <li>Repeat Password: <input name="txtPassword2" type="password" id="txtPassword2" maxlength="20" /></li>
	  <li>Email Address: <input name="txtEmail" type="text" id="txtEmail" maxlength="50" /></li>
    </ul>
	<input type="submit" name="SubmitAdd" value="Add User" />
  </form>
  <h3>Delete User</h3>
  <form id="frmDelUser" name="frmDelUser" method="post" action="admin/script/user.php">
    <ul>
      <li><select name="txtID" id="txtID">
	  <?PHP
		//Count number of categories in database
		$num=mysql_num_rows($result);
		
		//Print all categorys in list
		for ($i = 0; $i < $num; $i++) {
			echo '<option value="' . mysql_result($result,$i,"ID") . '" >' . stripslashes(mysql_result($result,$i,"Username")) . '</option>';
		}
		
		//Close Database
		mysql_close();
	  ?>
	  </select></li>
	</ul>
	<input type="submit" name="SubmitDel" value="Delete User" />
  </form>
  <p><a href="admin">Back to hub</a></p>
</body>
</html>