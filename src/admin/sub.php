<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include '../script/base.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <link href="files/style.css" rel="stylesheet" type="text/css" /> -->
<title>GBPS3 Admin - Edit Tips</title>
</head>

<body>
  <h2>GBPS3 Admin - Flip Sub Switch</h2>
  <?PHP
	if (isset($_GET["error"])) {
  ?>
  <h4>Error: Required field(s) "<?PHP echo $_GET["error"]; ?>" are missing or incorrect, please try again.</h4>
  <?PHP
	}
  ?>
  <form id="frmChooseVid" name="frmChooseVid" method="get" action="admin/sub.php">
    <ul>
	  <li>Video: <input name="id" type="text" id="id" maxlength="5" /></li>
    </ul>
	<input type="submit" name="Submit" value="Choose Video" />
  </form>
  
  <?PHP
	//If video has been chosen, then enable part 2
	if (!empty($_GET["id"])) {
		
		//connect to database
		define('DBTest', TRUE);
		include '../script/db.php';
		
		//Get sub information from gb_video for this video
		$sql = "SELECT Title,Member FROM gb_video WHERE ID = '" . $_GET["id"] . "'";
		$result = mysql_query($sql);
  ?>
  <form id="frmFlipSwitch" name="frmFlipSwitch" method="post" action="admin/script/sub.php">
  <input type="hidden" name="txtID" value=<?PHP echo $_GET["id"]; ?> />
    <ul>
	  <li>Video No. #<?PHP echo $_GET["id"] . " (" . mysql_result($result,0,"Title") . ")"; ?> is a 
	    <?PHP
			if (mysql_result($result,0,"Member") == 0)
			{
				echo "Non Subscriber";
			}
			else
			{
				echo "Subscriber";
			}
		?>
		 video. Flip the switch?
	  </li>
    </ul>
	<input type="submit" name="Submit" value="Flip Switch" />
  </form>
  <?PHP
	}
  ?>
  <p><a href="admin">Back to hub</a></p>
</body>
</html>