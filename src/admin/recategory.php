<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include '../script/base.php';
	
	//connect to database
	define('DBTest', TRUE);
	include '../script/db.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <link href="files/style.css" rel="stylesheet" type="text/css" /> -->
<title>GBPS3 Admin - Rearrange Categories</title>
</head>

<body>
  <h2>GBPS3 Admin - Rearrange Categories</h2>
  <?PHP
	if (isset($_GET["error"])) {
  ?>
  <h4>Error: Required field(s) "<?PHP echo $_GET["error"]; ?>" are missing or incorrect, please try again.</h4>
  <?PHP
	}
  ?>
  <h3>Features:</h3>
  <form id="frmFeat" name="frmFeat" method="post" action="admin/script/recategory.php">
    <ul>
	<?PHP
		//Get data from gb_category for features
		$sql = "SELECT * FROM gb_category WHERE Section = 1 ORDER BY OrderNo ASC";
		$result = mysql_query($sql);
		
		//Print for all categories
		$num = mysql_num_rows($result);
		for ($i = 0; $i < $num; $i++) {
			$id = mysql_result($result,$i,"ID");
			echo '<li>' . stripslashes(mysql_result($result,$i,"Name")) . ': <input name="txt' . $id . '" type="text" id="txt' . $id . '" maxlength="2" value="' . mysql_result($result,$i,"OrderNo") . '" /></li>';
		}
	?>
    </ul>
	<input type="submit" name="SubmitFeat" value="Change Features Order" />
  </form>
  <h3>Episodes:</h3>
  <form id="frmEp" name="frmEp" method="post" action="admin/script/recategory.php">
    <ul>
	<?PHP
		//Get data from gb_category for features
		$sql = "SELECT * FROM gb_category WHERE Section = 2 ORDER BY OrderNo ASC";
		$result = mysql_query($sql);
		
		//Print for all categories
		$num = mysql_num_rows($result);
		for ($i = 0; $i < $num; $i++) {
			$id = mysql_result($result,$i,"ID");
			echo '<li>' . stripslashes(mysql_result($result,$i,"Name")) . ': <input name="txt' . $id . '" type="text" id="txt' . $id . '" maxlength="2" value="' . mysql_result($result,$i,"OrderNo") . '" /></li>';
		}
	?>
    </ul>
	<input type="submit" name="SubmitEp" value="Change Episodes Order" />
  </form>
  <h3>Member Features:</h3>
  <form id="frmEp" name="frmEp" method="post" action="admin/script/recategory.php">
    <ul>
	<?PHP
		//Get data from gb_category for features
		$sql = "SELECT * FROM gb_category WHERE Section = 3 ORDER BY OrderNo ASC";
		$result = mysql_query($sql);
		
		//Print for all categories
		$num = mysql_num_rows($result);
		for ($i = 0; $i < $num; $i++) {
			$id = mysql_result($result,$i,"ID");
			echo '<li>' . stripslashes(mysql_result($result,$i,"Name")) . ': <input name="txt' . $id . '" type="text" id="txt' . $id . '" maxlength="2" value="' . mysql_result($result,$i,"OrderNo") . '" /></li>';
		}
		
		//close database
		mysql_close();
	?>
    </ul>
	<input type="submit" name="SubmitMemFeat" value="Change Member Features Order" />
  </form>
  <p><a href="admin">Back to hub</a></p>
</body>
</html>