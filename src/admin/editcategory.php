<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include '../script/base.php';
	
	//connect to database
	define('DBTest', TRUE);
	include '../script/db.php';
	
	//Get data from gb_feature
	$sql = "SELECT * FROM gb_category ORDER BY Name ASC";
	$result = mysql_query($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <link href="files/style.css" rel="stylesheet" type="text/css" /> -->
<title>GBPS3 Admin - Edit Category</title>
</head>

<body>
  <h2>GBPS3 Admin - Edit Category</h2>
  <?PHP
	if (isset($_GET["error"])) {
  ?>
  <h4>Error: Required field(s) "<?PHP echo $_GET["error"]; ?>" are missing or incorrect, please try again.</h4>
  <?PHP
	}
  ?>
  <form id="frmChooseCategory" name="frmChooseCategory" method="get" action="admin/editcategory.php">
    <ul>
	  <li>Choose Category: <select name="id" id="id">
	  <?PHP
		//Count number of categories in database
		$num = mysql_num_rows($result);
		
		//Print for all categories
		for ($i = 0; $i < $num; $i++) {
			$id = mysql_result($result,$i,"ID");
			
			echo '<option value="' .$id . '" ';
			if ($_GET["id"] == $id) {
				echo 'selected="selected" ';
			}
			echo '>' . stripslashes(mysql_result($result,$i,"Name")) . '</option>';
		}
	  ?>
	  </select></li>
	</ul>
	<input type='submit' name='SubmitEdit' value='Edit Category' />
    <br />
  </form>
  <?PHP
	if (!empty($_GET["id"])) {
		//Get data for selected category
		$sql = "SELECT * FROM gb_category WHERE ID = '" . $_GET["id"] . "'";
		$result = mysql_query($sql);
		
		//Saved for one time run
		$id = mysql_result($result,0,"Section");
		$sort = mysql_result($result,0,"Sort");
  ?>
  <form id="frmCategory" name="frmCategory" method="post" action="admin/script/editcategory.php">
    <ul>
	  <input type="hidden" name="txtOldID" value=<?PHP echo $_GET["id"]; ?> >
	  <li>Name: <input name="txtName" type="text" id="txtName" maxlength="60" <?PHP echo 'value="' . stripslashes(mysql_result($result,0,"Name")) . '"'; ?> /></li>
	  <li>ID: <input name="txtID" type="text" id="txtID" maxlength="10" <?PHP echo 'value="' . mysql_result($result,0,"ID") . '"'; ?> /></li>
	  <li>Section: <select name="txtSection" id="txtSection">
	               <option value="1" <?PHP if ($id == 1) {echo 'selected="selected"';} ?> >1</option>
				   <option value="2" <?PHP if ($id == 2) {echo 'selected="selected"';} ?> >2</option>
				   <option value="3" <?PHP if ($id == 3) {echo 'selected="selected"';} ?> >3</option>
				   </select></li>
	  <li>Code 1: <input name="txtCode1" type="text" id="txtCode1" maxlength="60" <?PHP echo 'value="' . stripslashes(mysql_result($result,0,"Code1")) . '"'; ?> /></li>
	  <li>Code 2: <input name="txtCode2" type="text" id="txtCode2" maxlength="60" <?PHP echo 'value="' . stripslashes(mysql_result($result,0,"Code2")) . '"'; ?> /></li>
	  <li>Sort: <select name="txtSort" id="txtSort">
	               <option value="ASC" <?PHP if ($sort == "ASC") {echo 'selected="selected"';} ?> >ASC</option>
				   <option value="DESC" <?PHP if ($sort == "DESC") {echo 'selected="selected"';} ?> >DESC</option>
				   </select></li>
    </ul>
	<input type="submit" name="Submit" value="Change Category" />
  </form>
  <?PHP
	}
	
	//close database
	mysql_close();
  ?>
  <p><a href="admin">Back to hub</a></p>
</body>
</html>