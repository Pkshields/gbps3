<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include '../script/base.php';
	
	//connect to database
	define('DBTest', TRUE);
	include '../script/db.php';
	
	//Get data from gb_feature
	$sql = "SELECT * FROM gb_feature ORDER BY ID ASC";
	$result = mysql_query($sql);
	
	//Store category names and num in array
	$catid = array();
	$catnum = array();
	for ($i = 0; $i < 6; $i++) {
		$catid[$i] = mysql_result($result,$i,"Category");
		$catnum[$i] = mysql_result($result,$i,"Num");
	}
	
	//Open database to gb_category
	$sql = "SELECT * FROM gb_category ORDER BY Name ASC";
	$result = mysql_query($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <link href="files/style.css" rel="stylesheet" type="text/css" /> -->
<title>GBPS3 Admin - Feature Management</title>
</head>

<body>
  <h2>GBPS3 Admin - Feature Management</h2>
  <?PHP
	if (isset($_GET["error"])) {
  ?>
  <h4>Error: Required field(s) "<?PHP echo $_GET["error"]; ?>" are missing or incorrect, please try again.</h4>
  <?PHP
	}
  ?>
  <form id="frmFeatures" name="frmFeatures" method="post" action="admin/script/feature.php">
  <?PHP
	//Count number of categories in database
	$num=mysql_num_rows($result);
	
	//store category names in array first
	$allcatname = array();
	$allcatid = array();
	for ($i = 0; $i < $num; $i++) {
		$allcatname[$i] = mysql_result($result,$i,"Name");
		$allcatid[$i] = mysql_result($result,$i,"ID");
	}
	
	//Close database
	mysql_close();
	
	//Print 6 categories with num (+ inital override at start)
	echo '<ul>';
	for ($i = 0; $i < 6; $i++) {
		//First, override true unless proven quilty
		$override = TRUE;

		//Start feature!
		echo '<li>Feature #' . ($i + 1) . ': ';
		echo '<select name="txtCatID' . $i . '" id="txtCatID' . $i . '">';
		
		//Create all options with default being whatever is in the database
		for ($j = 0; $j < $num; $j++) {
			echo '<option value="' . $allcatid[$j] . '" ';
			if ($allcatid[$j] == $catid[$i]) {
				echo 'selected="selected" ';
				$override = FALSE;
			}
			echo '>' . $allcatname[$j] . '</option>';
		}
		
		//Add override
		echo '<option value="override" ';
		if ($override) {
			echo 'selected="selected" ';
		}
		echo '> - Override</option>';
		
		//finish category option
		echo '</select> - ';
		
		//create number options with default
		echo '<select name="txtNum' . $i . '" id="txtNum' . $i . '">';
		
		//Create all options with default being whatever is in the database
		for ($j = 0; $j < 6; $j++) {
			echo '<option value="' . $j . '" ';
			if ($j == $catnum[$i]) {
				echo 'selected="selected" ';
			}
			echo '>' . $j . '</option>';
		}
		
		//Finish num option
		echo '</select> - ';
		
		//Create override text field
		echo '<input name="txtOver' . $i . '" type="text" id="txtOver' . $i . '" maxlength="60" ';
		if ($override) {
			echo 'value="' . stripslashes($catid[$i]) . '" ';
			$override = FALSE;
		}
		echo '/>';
		
		//finish override option
		echo '</li>';
	}
	echo '</ul>';
  ?>
	<p><input type='submit' name='Submit' value='Change Features' /></p>
  </form>
  <p><a href="admin">Back to hub</a></p>
</body>
</html>