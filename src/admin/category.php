<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include '../script/base.php';
	
	//connect to database
	define('DBTest', TRUE);
	include '../script/db.php';
	
	//Get data from gb_feature
	$sql = "SELECT * FROM gb_category ORDER BY Name ASC";
	$result = mysql_query($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <link href="files/style.css" rel="stylesheet" type="text/css" /> -->
<title>GBPS3 Admin - Category Management</title>
</head>

<body>
  <h2>GBPS3 Admin - Category Management</h2>
  <?PHP
	if (isset($_GET["error"])) {
  ?>
  <h4>Error: Required field(s) "<?PHP echo $_GET["error"]; ?>" are missing or incorrect, please try again.</h4>
  <?PHP
	}
  ?>
  <h3>Add Category</h3>
  <form id="frmCategory" name="frmCategory" method="post" action="admin/script/category.php">
    <ul>
	  <li>Name: <input name="txtName" type="text" id="txtName" maxlength="60" /></li>
	  <li>ID: <input name="txtID" type="text" id="txtID" maxlength="10" /></li>
	  <li>Section: <select name="txtSection" id="txtSection">
	               <option value="1" >1</option>
				   <option value="2" >2</option>
				   <option value="3" >3</option>
				   </select></li>
	  <li>Code 1: <input name="txtCode1" type="text" id="txtCode1" maxlength="60" /></li>
	  <li>Code 2: <input name="txtCode2" type="text" id="txtCode2" maxlength="60" /></li>
	  <li>Sort: <select name="txtSort" id="txtSort">
	               <option value="ASC" >ASC</option>
				   <option value="DESC" >DESC</option>
				   </select></li>
    </ul>
	<input type="submit" name="SubmitAdd" value="Add Category" />
  </form>
  <h3>Delete Category</h3>
  <form id="frmDelCategory" name="frmDelCategory" method="post" action="admin/script/category.php">
    <ul>
      <li><select name="txtID" id="txtID">
	  <?PHP
		//Count number of categories in database
		$num=mysql_num_rows($result);
		
		//Print all categorys in list
		for ($i = 0; $i < $num; $i++) {
			echo '<option value="' . mysql_result($result,$i,"ID") . '" >' . stripslashes(mysql_result($result,$i,"Name")) . '</option>';
		}
		
		//Close Database
		mysql_close();
	  ?>
	  </select></li>
	</ul>
	<input type="submit" name="SubmitDel" value="Delete Category" />
  </form>
  <p><a href="admin">Back to hub</a></p>
</body>
</html>