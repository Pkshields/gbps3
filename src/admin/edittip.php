<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include '../script/base.php';
	
	//connect to database
	define('DBTest', TRUE);
	include '../script/db.php';
	
	//Get data from gb_feature
	$sql = "SELECT * FROM gb_tip ORDER BY ID ASC";
	$result = mysql_query($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <link href="files/style.css" rel="stylesheet" type="text/css" /> -->
<title>GBPS3 Admin - Edit Tips</title>
</head>

<body>
  <h2>GBPS3 Admin - Edit Tips</h2>
  <?PHP
	if (isset($_GET["error"])) {
  ?>
  <h4>Error: Required field(s) "<?PHP echo $_GET["error"]; ?>" are missing or incorrect, please try again.</h4>
  <?PHP
	}
  ?>
  <form id="frmChooseTip" name="frmChooseTip" method="get" action="admin/edittip.php">
    <ul>
	  <li>Choose Tip: <select name="id" id="id">
	  <?PHP
		//Count number of categories in database
		$num = mysql_num_rows($result);
		
		//Print for all categories
		for ($i = 0; $i < $num; $i++) {
			$id = mysql_result($result,$i,"ID");
			
			echo '<option value="' .$id . '" ';
			if ($_GET["id"] == $id) {
				echo 'selected="selected" ';
			}
			echo '>' . mysql_result($result,$i,"Tip") . '</option>';
		}
	  ?>
	  </select></li>
	</ul>
	<input type='submit' name='SubmitEdit' value='Edit Tip' />
    <br />
  </form>
  <?PHP
	if (!empty($_GET["id"])) {
		//Get data for selected category
		$sql = "SELECT * FROM gb_tip WHERE ID = '" . $_GET["id"] . "'";
		$result = mysql_query($sql);
  ?>
  <form id="frmCategory" name="frmCategory" method="post" action="admin/script/edittip.php">
    <ul>
	  <input type="hidden" name="txtID" value=<?PHP echo $_GET["id"]; ?> >
	  <li>Tip: <input name="txtTip" type="text" id="txtTip" maxlength="200" value="<?PHP echo stripslashes(mysql_result($result,0,"Tip")); ?>" /></li>
    </ul>
	<input type="submit" name="Submit" value="Change Tip" />
  </form>
  <?PHP
	}
	
	//close database
	mysql_close();
  ?>
  <p><a href="admin">Back to hub</a></p>
</body>
</html>