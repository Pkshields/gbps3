<?PHP
	//Set redirects
	$redir = "../user.php";
	
	//redirect if not coming from addcust
	if(!isset($_POST['SubmitAdd']) && !isset($_POST['SubmitDel'])) { header("Location: " . $redir); die(); }
	
	//Connect to database
	define('DBTest', TRUE);
	include '../../script/db.php';
	
	//Run if data needs added to database
	if(isset($_POST['SubmitAdd'])) {
		//required fields error function
		define('ReqFieldTest', TRUE);
		include '../../script/reqfield.php';
		
		//Email check error function
		define('EmailTest', TRUE);
		include '../../script/checkemail.php';
		
		//Get new user info
		$username = trim($_POST['txtUsername']);
		$password1 = trim($_POST['txtPassword1']);
		$password2 = trim($_POST['txtPassword2']);
		$email = trim($_POST['txtEmail']);
		
		//fix sql injection
		$username = mysql_real_escape_string($username);
		$password1 = mysql_real_escape_string($password1);
		$password2 = mysql_real_escape_string($password2);
		$email = mysql_real_escape_string($email);
		
		//check required fields
		$error = required ($username, "Username", $error);
		$error = required ($password1, "Password1", $error);
		$error = required ($password2, "Password2", $error);
		$error = required ($email, "Email", $error);
		requiredcheck ($error, $redir);
		
		//Check email and passwords
		checkemail ($email, $redir);
		if ($password1 !== $password2) {
			die("Passwords are not the same");
		}
		
		//Encrypt passwords
		$password = md5($password1);
		
		//Add new user to database
		$sql = 'INSERT INTO gb_user(Username,Password,Email) ';
		$sql = $sql . 'VALUES ("' . $username . '","' . $password . '","' . $email . '") ';
		$result=mysql_query($sql);
	}
	elseif (isset($_POST['SubmitDel'])) {
		//Get category id
		$id = trim($_POST['txtID']);
		
		//fix sql injection
		$id = mysql_real_escape_string($id);
		
		//Delete category
		$sql = 'DELETE FROM gb_user WHERE ID = "' . $id . '"';
		$result=mysql_query($sql);
	}
	//Close Database
	mysql_close();
	
	//redirect to user
	header("Location: " . $redir);
?>