<?PHP
	//Set redirects
	$redir = "../edittip.php";
	
	//redirect if not coming from addcust
	if(!isset($_POST['SubmitEdit']) && !isset($_POST['Submit'])) { header("Location: " . $redir); die(); }
	
	//Connect to database
	define('DBTest', TRUE);
	include '../../script/db.php';
	
	//required fields error function
	define('ReqFieldTest', TRUE);
	include '../../script/reqfield.php';
	
	//Get new tip info
	$id = trim($_POST['txtID']);
	$tip = trim($_POST['txtTip']);
	
	//fix sql injection
	$id = mysql_real_escape_string($id);
	$tip = mysql_real_escape_string($tip);
	
	//check required fields
	$error = required ($id, "ID", $error);
	$error = required ($tip, "Tip", $error);
	requiredcheck ($error, $redir);
	
	//Edit tip in database
	$sql = 'UPDATE gb_tip SET Tip="' . $tip . '" WHERE ID ="' . $id . '"';
	$result=mysql_query($sql);
	
	//Close Database
	mysql_close();
	
	//redirect to tip
	header("Location: " . $redir);
?>