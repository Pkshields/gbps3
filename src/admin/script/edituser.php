<?PHP
	//Set redirects
	$redir = "../edituser.php";
	
	//redirect if not coming from addcust
	if(!isset($_POST['SubmitEdit']) && !isset($_POST['Submit'])) { header("Location: " . $redir); die(); }
	
	//Connect to database
	define('DBTest', TRUE);
	include '../../script/db.php';
	
	//required fields error function
	define('ReqFieldTest', TRUE);
	include '../../script/reqfield.php';
	
	//Email check error function
	define('EmailTest', TRUE);
	include '../../script/checkemail.php';
	
	//Get new user info
	$username = trim($_POST['txtUsername']);
	$password1 = trim($_POST['txtPassword1']);
	$password2 = trim($_POST['txtPassword2']);
	$email = trim($_POST['txtEmail']);
	$id = trim($_POST['txtID']);
	
	//fix sql injection
	$username = mysql_real_escape_string($username);
	$password1 = mysql_real_escape_string($password1);
	$password2 = mysql_real_escape_string($password2);
	$email = mysql_real_escape_string($email);
	$id = mysql_real_escape_string($id);
	
	//check required fields
	$error = required ($username, "Username", $error);
	$error = required ($email, "Email", $error);
	$error = required ($id, "ID", $error);
	requiredcheck ($error, $redir);
	
	//Check email and passwords (if applicable)
	checkemail ($email, $redir);
	if ($password1 !== $password2 && !empty($password1)) {
		die("Passwords are not the same");
	}
	
	//Encrypt passwords
	if (!empty($password1)) {
		$password = md5($password1);
	}
	
	//Edit user in database
	$sql = 'UPDATE gb_user SET Username="'. $username . '", Email ="'. $email . '"';
	if (!empty($password1)) {
		$sql = $sql . ', Password ="' . $password . '"';
	}
	$sql = $sql . ' WHERE ID ="' . $id . '"';
	$result=mysql_query($sql);
	
	//Close Database
	mysql_close();
	
	//redirect to edituser
	header("Location: " . $redir);
?>