<?PHP
	//Set redirects
	$redir = "../tip.php";
	
	//redirect if not coming from addcust
	if(!isset($_POST['SubmitAdd']) && !isset($_POST['SubmitDel'])) { header("Location: " . $redir); die(); }
	
	//Connect to database
	define('DBTest', TRUE);
	include '../../script/db.php';
	
	//Run if data needs added to database
	if(isset($_POST['SubmitAdd'])) {
		//required fields error function
		define('ReqFieldTest', TRUE);
		include '../../script/reqfield.php';
		
		//Get new tip info
		$tip = trim($_POST['txtTip']);
		
		//fix sql injection
		$tip = mysql_real_escape_string($tip);
		
		//check required fields
		$error = required ($tip, "Tip", $error);
		requiredcheck ($error, $redir);
		
		//Add new tip to database
		$sql = 'INSERT INTO gb_tip(tip) ';
		$sql = $sql . 'VALUES ("' . $tip . '") ';
		$result=mysql_query($sql);
	}
	elseif (isset($_POST['SubmitDel'])) {
		//Get tip id
		$id = trim($_POST['txtID']);
		
		//fix sql injection
		$id = mysql_real_escape_string($id);
		
		//Delete tip
		$sql = "DELETE FROM gb_tip WHERE ID = '" . $id . "'";
		$result=mysql_query($sql);
	}
	//Close Database
	mysql_close();
	
	//redirect to tip
	header("Location: " . $redir);
?>