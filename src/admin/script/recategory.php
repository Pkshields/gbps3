<?PHP
	//Set redirects
	$redir = "../recategory.php";
	
	//redirect if not coming from addcust
	if(!isset($_POST['SubmitFeat']) && !isset($_POST['SubmitEp'])&& !isset($_POST['SubmitMemFeat'])) { header("Location: " . $redir); die(); }
	
	//Connect to database
	define('DBTest', TRUE);
	include '../../script/db.php';
	
	//required fields error function
	define('ReqFieldTest', TRUE);
	include '../../script/reqfield.php';
	
	//Items to change if it is either category
	if (isset($_POST['SubmitFeat'])) {
		$section = 1;
	}
	elseif (isset($_POST['SubmitEp'])) {
		$section = 2;
	}
	elseif (isset($_POST['SubmitMemFeat'])) {
		$section = 3;
	}
	
	//Access database
	$sql = "SELECT * FROM gb_category WHERE Section = " . $section . " ORDER BY OrderNo ASC";
	$result = mysql_query($sql);
	
	//Store all IDs in database
	$num = mysql_num_rows($result);
	$id = array();
	for ($i = 0; $i < $num; $i++) {
		$id[$i] = mysql_result($result,$i,"ID");
	}
	
	//Get new order info + fix sql injection + check required fields
	$order = array();
	for ($i = 0; $i < $num; $i++) {
		$order[$id[$i]] = trim($_POST['txt' . $id[$i]]);
		$order[$id[$i]] = mysql_real_escape_string($order[$id[$i]]);
		$error = required ($order[$id[$i]], "".$id[$i], $error);
		requiredcheck ($error, $redir);
	}
	
	//Add changes to database
	for ($i = 0; $i < $num; $i++) {
		$sql = "UPDATE gb_category SET OrderNo='". $order[$id[$i]] . "' WHERE ID ='" . $id[$i] . "'";
		$result=mysql_query($sql);
	}
	
	//Close Database
	mysql_close();
	
	//redirect to category
	header("Location: " . $redir);
?>