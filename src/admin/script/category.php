<?PHP
	//Set redirects
	$redir = "../category.php";
	
	//redirect if not coming from addcust
	if(!isset($_POST['SubmitAdd']) && !isset($_POST['SubmitDel'])) { header("Location: " . $redir); die(); }
	
	//Connect to database
	define('DBTest', TRUE);
	include '../../script/db.php';
	
	//Run if data needs added to database
	if(isset($_POST['SubmitAdd'])) {
		//required fields error function
		define('ReqFieldTest', TRUE);
		include '../../script/reqfield.php';
		
		//Get new category info
		$name = trim($_POST['txtName']);
		$id = trim($_POST['txtID']);
		$section = trim($_POST['txtSection']);
		$code1 = trim($_POST['txtCode1']);
		$code2 = trim($_POST['txtCode2']);
		$sort = trim($_POST['txtSort']);
		
		//fix sql injection
		$name = mysql_real_escape_string($name);
		$id = mysql_real_escape_string($id);
		$section = mysql_real_escape_string($section);
		$code1 = mysql_real_escape_string($code1);
		$code2 = mysql_real_escape_string($code2);
		$sort = mysql_real_escape_string($sort);
		
		//check required fields
		$error = required ($name, "Name", $error);
		$error = required ($id, "ID", $error);
		$error = required ($section, "Section", $error);
		$error = required ($code1, "Code 1", $error);
		$error = required ($sort, "Sort", $error);
		requiredcheck ($error, $redir);
		
		//Add new category to database
		if (!empty($code2)) {
			$sql = 'INSERT INTO gb_category(ID,Name,Section,OrderNo,Code1,Code2,Sort) ';
		}
		else
		{
			$sql = 'INSERT INTO gb_category(ID,Name,Section,OrderNo,Code1,Sort) ';
		}
		$sql = $sql . 'VALUES ("' . $id . '","' . $name . '",' . $section . ',99,"' . $code1 . '"';
		if (!empty($code2)) {
			$sql = $sql . ',"' . $code2 . '"';
		}
		$sql = $sql . ',"' . $sort . '") ';
		$result=mysql_query($sql);
	}
	elseif (isset($_POST['SubmitDel'])) {
		//Get category id
		$id = trim($_POST['txtID']);
		
		//fix sql injection
		$id = mysql_real_escape_string($id);
		
		//Delete category
		$sql = 'DELETE FROM gb_category WHERE ID = "' . $id . '"';
		$result=mysql_query($sql);
	}
	//Close Database
	mysql_close();
	
	//redirect to category
	header("Location: " . $redir);
?>