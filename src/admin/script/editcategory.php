<?PHP
	//Set redirects
	$redir = "../editcategory.php";
	
	//redirect if not coming from addcust
	if(!isset($_POST['SubmitEdit']) && !isset($_POST['Submit'])) { header("Location: " . $redir); die(); }
	
	//Connect to database
	define('DBTest', TRUE);
	include '../../script/db.php';
	
	//required fields error function
	define('ReqFieldTest', TRUE);
	include '../../script/reqfield.php';
	
	//Get new category info
	$name = trim($_POST['txtName']);
	$id = trim($_POST['txtID']);
	$section = trim($_POST['txtSection']);
	$code1 = trim($_POST['txtCode1']);
	$code2 = trim($_POST['txtCode2']);
	$sort = trim($_POST['txtSort']);
	$oldid = trim($_POST['txtOldID']);
	
	//fix sql injection
	$name = mysql_real_escape_string($name);
	$id = mysql_real_escape_string($id);
	$section = mysql_real_escape_string($section);
	$code1 = mysql_real_escape_string($code1);
	$code2 = mysql_real_escape_string($code2);
	$sort = mysql_real_escape_string($sort);
	$oldid = mysql_real_escape_string($oldid);
	
	//check required fields
	$error = required ($name, "Name", $error);
	$error = required ($id, "ID", $error);
	$error = required ($section, "Section", $error);
	$error = required ($code1, "Code 1", $error);
	$error = required ($sort, "Sort", $error);
	$error = required ($oldid, "Old ID", $error);
	
	//Edit category in database
	$sql = 'UPDATE gb_category SET ID="'. $id . '", Name ="'. $name . '", Section ="' . $section . '", Code1 ="' . $code1 . '", Sort ="' . $sort . '"';
	if (!empty($code2)) {
		$sql = $sql . ', Code2 ="' . $code2 . '"';
	}
	$sql = $sql . ' WHERE ID ="' . $oldid . '"';
	$result=mysql_query($sql);
	
	//Close Database
	mysql_close();
	
	//redirect to category
	header("Location: " . $redir);
?>