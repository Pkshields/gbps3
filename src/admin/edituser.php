<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include '../script/base.php';
	
	//connect to database
	define('DBTest', TRUE);
	include '../script/db.php';
	
	//Get data from gb_feature
	$sql = "SELECT * FROM gb_user ORDER BY Username ASC";
	$result = mysql_query($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <link href="files/style.css" rel="stylesheet" type="text/css" /> -->
<title>GBPS3 Admin - Edit User</title>
</head>

<body>
  <h2>GBPS3 Admin - Edit User</h2>
  <?PHP
	if (isset($_GET["error"])) {
  ?>
  <h4>Error: Required field(s) "<?PHP echo $_GET["error"]; ?>" are missing or incorrect, please try again.</h4>
  <?PHP
	}
  ?>
  <form id="frmChooseUser" name="frmChooseUser" method="get" action="admin/edituser.php">
    <ul>
	  <li>Choose User: <select name="id" id="id">
	  <?PHP
		//Count number of categories in database
		$num = mysql_num_rows($result);
		
		//Print for all categories
		for ($i = 0; $i < $num; $i++) {
			$id = mysql_result($result,$i,"ID");
			
			echo '<option value="' .$id . '" ';
			if ($_GET["id"] == $id) {
				echo 'selected="selected" ';
			}
			echo '>' . stripslashes(mysql_result($result,$i,"Username")) . '</option>';
		}
	  ?>
	  </select></li>
	</ul>
	<input type='submit' name='SubmitEdit' value='Edit User' />
    <br />
  </form>
  <?PHP
	if (!empty($_GET["id"])) {
		//Get data for selected category
		$sql = "SELECT * FROM gb_user WHERE ID = '" . $_GET["id"] . "'";
		$result = mysql_query($sql);
  ?>
  <form id="frmUser" name="frmUser" method="post" action="admin/script/edituser.php">
    <ul>
	  <input type="hidden" name="txtID" value=<?PHP echo $_GET["id"]; ?> >
	  <li>Username: <input name="txtUsername" type="text" id="txtUsername" maxlength="20" <?PHP echo 'value="' . stripslashes(mysql_result($result,0,"Username")) . '"'; ?> /></li>
	  <li>Password: <input name="txtPassword1" type="password" id="txtPassword1" maxlength="20" /></li>
	  <li>Repeat Password: <input name="txtPassword2" type="password" id="txtPassword2" maxlength="20" /></li>
	  <li>Email Address: <input name="txtEmail" type="text" id="txtEmail" maxlength="50" <?PHP echo 'value="' . stripslashes(mysql_result($result,0,"Email")) . '"'; ?> /></li>
    </ul>
	<input type="submit" name="Submit" value="Change User" />
  </form>
  <br />
  <dd>NOTE: Only enter the password fields if you want to change them.</dd>
  <?PHP
	}
	
	//close database
	mysql_close();
  ?>
  <p><a href="admin">Back to hub</a></p>
</body>
</html>