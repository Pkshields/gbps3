<?PHP
	//make sure it is not direct linking - security!
	if (!defined('ReqFieldTest')) {
		header("HTTP/1.1 404 File Not Found", 404);
		exit;
	}

	//required fields error function
	function required ($field, $name, $error) {
		//Check if the entered field is empty or not
		//!= 0 Added because 0 counts as empty
		if (empty($field) && $field != 0) {
			//It is! IF an error exists, add to that. Else, create a new one
			if (empty($error)) {
				$error = $name;
			}
			else {
				$error = $error . ", " . $name;
			}
		}
		
		//Return the error, created or not
		return $error;
	}
	
	//Error check function
	function requiredcheck ($error, $redir) {
		//If an error exists, redirect back to page without any changes and an error message
		if (!empty($error)) {
			header("Location: " . $redir . "?error=" . $error);
			die();
		}
	}
	
	
	
	
	
	
	function requireddie ($field, $name) {
		if (empty($field)) {
			die("Error: Required field " . $name . " missing, please try again.");
		}
	}
	
	function emptyreq ($error) {
		if (!empty($error)) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
?>