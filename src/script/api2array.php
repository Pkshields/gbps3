<?PHP
	//make sure it is not direct linking - security!
	if (!defined('APITest')) {
		header("HTTP/1.1 404 File Not Found", 404);
		exit;
	}

	//API 2 Array Script
	function api2array ($gb) {
		$gb = str_replace(" ", "+", $gb);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $gb);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    
		$json = curl_exec($ch);
		$data = json_decode($json, true);
		curl_close($ch);
		return $data;
	}
?>