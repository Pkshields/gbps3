<?PHP
	//make sure it is not direct linking - security!
	if (!defined('LoginTest')) {
		header("HTTP/1.1 404 File Not Found", 404);
		exit;
	}
	
	//Login check function
	function logincheck() {
		//Start session
		session_start();
		
		//Check if session already exists
		if (!empty($_SESSION["isdudeloggedin"]))
		{
			//Session has already been started, is dude logged in?
			if ($_SESSION["isdudeloggedin"] == "4101bef8794fed986e95dfb54850c68b")
			{
				//Not a member, return answer
				return FALSE;
			}
			else if ($_SESSION["isdudeloggedin"] == md5($_COOKIE["gb_login"]))
			{
				//Member, return answer
				return TRUE;
			}
			else
			{
				//Bad session variable, remove it
				unset($_SESSION["isdudeloggedin"]);
				return FALSE;
			}
		}
		
		//No session started, check if cookie exists
		if (empty($_COOKIE["gb_login"]))
		{
			//Cookie not set, not logged in. Set session too, so don't need this recheck?
			$_SESSION["isdudeloggedin"] = "4101bef8794fed986e95dfb54850c68b";
			return FALSE;
		}
		
		//No session, Cookie set, check database
		else
		{
			//Connect to database
			define('DBTest', TRUE);
			include 'db.php';
			
			//Parse cookie
			$id = intval(mysql_real_escape_string(substr($_COOKIE["gb_login"], -1, 1)));
			$hash = mysql_real_escape_string(substr($_COOKIE["gb_login"], 0, 32));
			
			//Check database for a thing
			$sql = "SELECT ID,Date FROM gb_login WHERE Hash = '" . $hash . "'";
			$result = mysql_query($sql);
			
			//Check entries
			$numrows = mysql_num_rows($result);
			
			if ($numrows == 1)
			{
				//Get ID in database
				$id2 = mysql_result($result,0,"ID");
				
				//Could be correct, check ID
				if ($id == $id2)
				{
					//Correct! Set session so this doesn't need to happen again
					$_SESSION["isdudeloggedin"] = md5($_COOKIE["gb_login"]);
					
					//While were here, check date to see if it needs to be renewed
					if (mysql_result($result,0,"Date") != date("Y-m-d"))
					{
						//Refresh database date and cookie
						setcookie("gb_login", $_COOKIE["gb_login"], time()+60*60*24*14, "/");
						$sql = 'UPDATE gb_login SET Date ="'. date("Y-m-d") . '"';
						$sql = $sql . ' WHERE Hash ="' . $hash . '"';
						$result = mysql_query($sql);
					}
					//End...
					mysql_close();
					return TRUE;
				}
			}
			else
			{
				//Don't know how you go here, don't care. Remove cookie, don't allow
				setcookie("gb_login", $_COOKIE["gb_login"], time() - 1, "/");
				$_SESSION["isdudeloggedin"] = "4101bef8794fed986e95dfb54850c68b";
				mysql_close();
				return FALSE;
			}
		}
		
		//Impossible to get here, but just in case
		return FALSE;
	}
?>