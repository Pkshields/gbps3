<?PHP
	//make sure it is not direct linking - security!
	if (!defined('DBTest')) {
		header("HTTP/1.1 404 File Not Found", 404);
		exit;
	}

	//database settings
	$dbhost = 'localhost';
	$dbuser = 'root';
	$dbpass = 'root';
	$dbname = 'gbps3';

	//connect to database
	$dbh = mysql_connect($dbhost,$dbuser,$dbpass);

	//If you cannot connect to the database, connect to backup
	$mysqlerror = 0;
	if (mysql_errno() != 0)
	{
		//backup datbase settings
		// 2019: Removed credentials
		$dbhost = '';
		$dbuser = '';
		$dbpass = '';
		$dbname = '';

		//connect to backup database
		$dbh = mysql_connect($dbhost,$dbuser,$dbpass);

		//If that fails too...
		if (mysql_errno() != 0)
		{
			$mysqlerror = 1;
		}
		//If sucessful, echo a statement so people know its running off a backup
		else
		{
			echo "GBPS3 is currently experiencing technical difficulties and is running from a backup database. Some of the videos may not show up on their category pages, but will show up on search";
		}
	}

	mysql_select_db($dbname, $dbh);

	//Error?
	if ($mysqlerror == 1) {
		header('Location: error/2303');
		die();
	}
?>
