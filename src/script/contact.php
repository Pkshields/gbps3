<?php
	header("Cache-control: private");
	if(!isset($_POST["Send"])) { header("Location: ../contactus" ); die(); }

	//Captcha code
	// 2019: Removed recaptcha private key
	require_once('recaptchalib.php');
	$privatekey = "";
	$resp = recaptcha_check_answer ($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

	if (!$resp->is_valid) {
		header("Location: ../contactus/error/captcha" );
		$finaltest = TRUE;
	}

	//Required Field script
	define('ReqFieldTest', TRUE);
	include 'reqfield.php';

	//Check Email script
	define('EmailTest', TRUE);
	include 'checkemail.php';

	//Save info first
	$name = trim(addslashes($_POST["txtName"]));
	$email = trim(addslashes($_POST["txtEmail"]));
	$subject = trim(addslashes($_POST["txtSubject"]));
	$comment = trim(addslashes($_POST["txtComments"]));

	//Check required fields
	$error = required($name, "Name", $error);
	$error = required($email, "Email", $error);
	$error = required($subject, "Subject", $error);
	$error = required($comment, "Comment", $error);

	if (emptyreq($error)) {
		session_start();
		header("Location: ../contactus/error/req");
		$finaltest = TRUE;
		$_SESSION["Error"] = $error;
	}

	//Check email is valid
	$validator = new EmailAddressValidator;
	if (!($validator->check_email_address($email)) && !$finaltest) {
		session_start();
		header("Location: ../contactus/error/email");
		$finaltest = TRUE;
	}

	//If error, save details
	if ($finaltest) {
		$_SESSION["Name"] = $name;
		$_SESSION["Email"] = $email;
		$_SESSION["Subject"] = $subject;
		$_SESSION["Comment"] = $comment;
		die();
	}

	//Define details
	$to = 'admin@muzene.com';
	$headers = "From: " . $email . "\r\nReply-To: " . $email;

	//define the subject + message of the email
	$message = $comment . "\n\nName: " . $name;

	//send the email
	$mail_sent = @mail( $to, $subject, $message, $headers );

	//if the message is sent successfully print "Mail sent". Otherwise print "Mail failed"
	//echo $mail_sent ? "Mail sent" : "Mail failed";
	header("Location: ../contactus/sent");
?>
