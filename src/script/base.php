<?PHP
	// 2019: Disabling warnings. We know this app is insecure now ;D
	error_reporting(E_ERROR);

	//make sure it is not direct linking - security!
	if (!defined('BaseTest')) {
		header("HTTP/1.1 404 File Not Found", 404);
		exit;
	}

	//BaseURL Script
	// 2019: Changed URL to the Vagrant IP
	function baseurl() {
		echo '<base href="http://192.168.33.10/" />';
	}
	$baseurl = "http://192.168.33.10/";
?>
