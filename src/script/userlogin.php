<?PHP
	//Set redirects
	$redir = "../login";
	
	//redirect if not coming from addcust
	if(!isset($_POST['SubmitIn']) && !isset($_POST['SubmitOut']) && !isset($_POST['SubmitPass'])) { header("Location: " . $redir); die(); }
	
	//Connect to database
	define('DBTest', TRUE);
	include 'db.php';
	
	//required fields error function
	define('ReqFieldTest', TRUE);
	include 'reqfield.php';
	
	//If login
	if (isset($_POST['SubmitIn']))
	{
		//Get new user info
		$username = trim($_POST['txtUsername']);
		$password = trim($_POST['txtPassword']);
			
		//fix sql injection
		$username = mysql_real_escape_string($username);
		$password = mysql_real_escape_string($password);
			
		//check required fields
		$error = required ($username, "Username", $error);
		$error = required ($password, "Password", $error);
		requiredcheck ($error, $redir);
			
		//Encrypt passwords
		$password = md5($password);
		
		//Check password with the database
		$sql = "SELECT ID, Password FROM gb_user WHERE Username = '" . $username . "'";
		$result = mysql_query($sql);
		
		if (mysql_result($result,0,"Password") == $password)
		{
			//If yes, generate md5 for the database
			$md5 = $username . date("m.d.y") . "gbps3";
			$md5 = md5($md5);
			
			//Insert into database
			$id = mysql_result($result,0,"ID");
			$sql = 'INSERT INTO gb_login(ID,Hash,Date) ';
			$sql = $sql . 'VALUES ("' . $id . '","' . $md5 . '","' . date("Y-m-d") . '")';
			$result = mysql_query($sql);
			
			//Create cookie
			//Cookie needs to contain the ID and hash
			//Login lasts 2 weeks without refresh
			setcookie("gb_login", $md5 . $id, time()+60*60*24*14, "/");
			
			//Clear the (now incorrect) session variable
			session_start();
			unset($_SESSION["isdudeloggedin"]);
		}
		else
		{
			//Temp redirect if login in incorrect
			$error = "Incorrect Password";
			mysql_close();
			requiredcheck ($error, $redir);
		}
	}
	//Else, if logout is set
	elseif (isset($_POST['SubmitOut']))
	{
		//Info from cookie
		$id = substr($_COOKIE["gb_login"], -1, 1);
		$hash = substr($_COOKIE["gb_login"], 0, 32);
		
		//Remove the entry in the database
		$sql = "DELETE FROM gb_login WHERE ID = '" . $id . "' AND Hash = '" . $hash . "'";
		$result=mysql_query($sql);
		
		//Remove the cookie
		setcookie("gb_login", $_COOKIE["gb_login"], time() - 1, "/");
		
		//Clear the (now incorrect) session variable
		session_start();
		unset($_SESSION["isdudeloggedin"]);
	}
	//Else, if change password is set
	elseif (isset($_POST['SubmitPass']))
	{
		//Info from cookie
		$id = substr($_COOKIE["gb_login"], -1, 1);
		
		//Get new password info
		$password1 = trim($_POST['txtPassword']);
		$password2 = trim($_POST['txtRepeatPassword']);
		
		//fix sql injection
		$password1 = mysql_real_escape_string($password1);
		$password2 = mysql_real_escape_string($password2);
		
		//check required fields
		$error = required ($password1, "Password1", $error);
		$error = required ($password2, "Password2", $error);
		
		//TEMP: Make sure the passwords are the same
		//Fix the die into a proper error message
		if ($password1 !== $password2) {
			die("Passwords are not the same");
		}
		
		//Encrypt passwords
		$password = md5($password1);
		
		//Remove the cookie
		setcookie("gb_login", $_COOKIE["gb_login"], time() - 1, "/");
		
		//Clear the (now incorrect) session variable
		session_start();
		unset($_SESSION["isdudeloggedin"]);
	}
	
	//Close Database
	mysql_close();
	
	//redirect to user
	header("Location: " . $redir);
?>