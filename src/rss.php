<?php
	//Base URL
	define('BaseTest', TRUE);
	include 'script/base.php';
	
	//connect to database
	define('DBTest', TRUE);
	include 'script/db.php';
	
	//Get videos from database
	$sql = "SELECT ID, Title, DATE_FORMAT(Date,'%a, %e %b %Y %T') as RSSDate FROM gb_video ORDER BY Date DESC LIMIT 30";
	$result = mysql_query($sql);
	
	//RSS Stuff
	header('Content-Type: text/xml');
	echo '<?xml version="1.0" encoding="ISO-8859-1"?>
	<rss version="2.0">
	<channel>
	<title>GBPS3</title>
	<description>Kitchen sink feed for GBPS3</description>
	<link>http://gbps3.muzene.com/</link>';

	//Convert SQL to Array for easy access
	$video = mysql_fetch_array($result);
	
	//Add all items to rss
	while($video = mysql_fetch_array($result)) {
		echo '
		<item>
			<title>' . $video[Title] . '</title>
			<description><![CDATA[' . $video[Title] . ']]></description>
			<link>http://gbps3.muzene.com/player/' . $video[ID] . '</link>
			<pubDate>' . $video[RSSDate] . ' EST</pubDate>
		</item>';
	}
	
	//Finish
	echo '</channel>
	</rss>';
?>