<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Redirecting...</title>
<meta http-equiv="REFRESH" content="0;url=../index.php">
<SCRIPT LANGUAGE="javascript">

// PS3 full display Version 3.0 04/04/2010

// This simple version of JavaScript in a run one state
// In other words, it will run only once you load you HTML in to PS3
// Why do platform check for PS3
// If do not do this people who use other systems ie Windows, Mac or (touchy subject of) Linux
// Then Browser will auto resize and it affect there enjoyment of the site
// There is a issue with flash video, if display video in Full display then exit
// then display on PS3 will look like 4:3.

// To use this just copy as is and place in to HTML in side BODY section of HTML

funtargetplatformresize()

function funtargetplatformcheck()
// This will check if the system OS [navigator.platform]
// It will return [true] if listed in [thetargetplatform]
{
	var thetargetplatform=["PLAYSTATION 3","PLAYSTATION3"]
	// Why do the check for "PLAYSTATION 3","PLAYSTATION3"?
	// Sony changed this back and forth with firmware 2.40 and
	// 2.50 so doing it just in case they change it again
	// If wanted to check for Windows just add "WIN32" etc
	var thelooptargetplatform=0
	var varthetargetplatformcheck=false
	for (thelooptargetplatform ; thelooptargetplatform<thetargetplatform.length ; thelooptargetplatform++) 
	{
		switch (navigator.platform.toUpperCase()) {
		case thetargetplatform[thelooptargetplatform].toUpperCase() : 
			varthetargetplatformcheck=true
		break
		default :
		}
	}
return(varthetargetplatformcheck)
}

function funtargetplatformresize()
// This will resize the browser screen to it maxium size
// if this OS is registered in [funtargetplatformcheck] and returns [true] // note: resize/move the browser can up set some people. That
// why it only being done here with PS3.

{
	if (funtargetplatformcheck()==true)
	{
		moveTo(0,0)
		resizeTo(screen.availWidth,screen.availHeight)
	}
}
</SCRIPT>
</head>
<p>Redirecting...</p>
<body>
</body>
</html>
