<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include 'script/base.php';

	//Login check - always before database connection!
	define('LoginTest', TRUE);
	include 'script/logincheck.php';
	$member = logincheck();

	//Connect to database
	define('DBTest', TRUE);
	include 'script/db.php';

	//Get API2Array Script
	define('APITest', TRUE);
	include 'script/api2array.php';

	//Get URL/ID
	$link = $_GET['v'];

	// Check if it is a URL or ID
	if (!ctype_digit($link)) {
		//Get rid of URL parts
		$link = strstr($link, '17-');
		$link = substr($link, 3);
		$link = stripslashes($link);
		$link = preg_replace('/[^a-z0-9\\s]/i', '', $link);
	}

	//make sure link isn't empty
	if (empty($link)) {
		header('Location: ' . $baseurl . 'error/2301');
		die();
	}

	//Ping API and get info
	$gb = "http://www.giantbomb.com/api/video/" . $link . "/?api_key=71ae30f6cdeb1c8d58aee42f9d0647cafefdabca &field_list=name,url,image,deck&format=json";
	$data = api2array($gb);

	//ID doesn't exist?
	if ($data['error'] == "Object Not Found") {
		header('Location: ' . $baseurl . 'error/2302');
		die();
	}

	//Check if it is a members video or not
	$sql = "SELECT Member FROM gb_video WHERE ID = '" . $link . "'";
	$result = mysql_query($sql);
	$memcheck = mysql_result($result,0,"Member");

	//Cookie check
	if ($_GET["q"] == "hq" || $_GET["q"] == "lq") {
		setcookie("gb_quality", $_GET["q"], time()+60*60*24*365, "/");
	}

	//Cookie set if default player set
	if ($_GET["d"] == "1") {
		setcookie("gb_player", "strplayer", time()+60*60*24*365, "/");
	}

	//Get video data and URL depending on if it is HQ or not
	$flv = ($_COOKIE["gb_quality"] == "hq" ? $data['results']['high_url'] : $data['results']['low_url']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="GBPS3: Watch <?PHP echo $data['results']['name']; ?> right from your PS3!" />
<meta name="KeyWords" content="gbps3, gb, ps3, giantbomb, giant, bomb, <?PHP echo $data['results']['name']; ?>" />
<link rel="icon" type="image/x-icon" href="image/favicon.ico" />
<link href="files/style.css" rel="stylesheet" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/gbps3" />
<title>GBPS3: Player - <?PHP echo $data['results']['name']; ?></title>
</head>

<body>

<?php include_once("script/analytics.php") ?>

<table id="table" width="1080" border="0" cellspacing="0">
  <tr>
    <td class="titleborder"><table width="1208" border="0">
      <tr>
        <td width="258"><a href="index"><img src="image/logo.gif" width="233" height="85" alt="GBPS3 Logo" /></a></td>
        <td width="622" align="center">Video: <?PHP echo $data['results']['name']; ?></td>
        <td width="314" class="titleright"><form id="frmSearch" name="frmSearch" method="get" action="redirect.php">
          <p>
            <input class="titletext"type="text" name="search" id="search" />
            <br />
<input type="submit" name="Submit" id="Submit" value="Search" />
          </p>
</form></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="middleborderbutton">
      <a href="index">
        <div class="button">
          <div class="buttontext">Home</div>
        </div>
      </a>
      <a href="categories">
        <div class="button">
          <div class="buttontext">Categories</div>
        </div>
      </a>
      <!-- <a href="test.php"> -->
        <div class="button">
          <div class="buttontext">&nbsp;</div>
        </div>
      <!-- </a> -->
      <a href="whiskeyplayer/<?PHP echo $link; ?>">
        <div class="button">
          <div class="buttontext">Whiskey</div>
        </div>
      </a>
      <a href="player/<?PHP echo $link; ?>">
        <div class="button">
          <div class="buttontext">Progressive</div>
        </div>
      </a>
    </td>
  </tr>
  <tr>
    <td class="middleborder">
      <div align="center">
        <p>
		<?PHP
			if ($memcheck == 0 || ($memcheck == 1 && $member)) {
		?>
	    <script type='text/javascript' src='files/playerswf.js'></script>
		<div id='mediaspace'>Not loading again? Comes and goes when it wants on PS3 sometimes, trying to find fix asap. Try clearing cache/reloading or try the Progressive Player</div>
	    <script type='text/javascript'>
	    var so = new SWFObject('files/strplayer.swf','mpl','640','385','9');
	    so.addParam('allowfullscreen','true');
	    so.addParam('allowscriptaccess','always');
	    so.addParam('wmode','opaque');
	    so.addVariable('author','<?PHP echo $data['results']['user']; ?>');
	    so.addVariable('description','<?PHP echo $data['results']['deck']; ?>');
	    so.addVariable('file','video/<?PHP echo $flv; ?>');
	    so.addVariable('image','<?PHP echo $data['results']['image']['super_url']; ?>');
	    so.addVariable('title','<?PHP echo $data['results']['name']; ?>');
	    so.addVariable('provider','rtmp');
	    so.addVariable('backcolor','000000');
	    so.addVariable('frontcolor','FFFFFF');
	    so.addVariable('lightcolor','CCCC00');
	    so.addVariable('screencolor','000000');
		so.addVariable('controlbar','over');
	    so.addVariable('streamer','rtmp://video.giantbomb.com/cfx/st/');
	    so.write('mediaspace');
	    </script>
		<?PHP
			}
			else {
		?>
		<p>Sorry, this video is for Premium Whiskey Media members only</p>
        <?PHP
			}

			if ((isset($_COOKIE["gb_player"]) && $_COOKIE["gb_player"] == "strplayer") || $_GET["d"] == "1") {
				echo '<div class="floatright">Default player set</div>';
			}
			else {
				echo '<div class="floatright"><a href="strplayer/' . $link . '/1">Set default player</a></div>';
			}
		?>
		<br />
        </p>
      </div>
    </td>
  </tr>
  <tr>
    <td class="bottomborder">
      <div class="floatleft">Description: <?PHP echo $data['results']['deck']; ?></div>
	  <?PHP
		if ($_GET["q"] == "hq" || ($_COOKIE["gb_quality"] == "hq" && $_GET["q"] !== "lq")) {
			echo '<div class="floatright">Quality: High (<a href="strplayer/' . $link . '/lq">Low?</a>)</div>';
		}
		else {
			echo '<div class="floatright">Quality: Low (<a href="strplayer/' . $link . '/hq">High?</a>)</div>';
		}
		?>
	</td>
  </tr>
</table>
<p align="center"><a href="http://muzene.com/">Muzene.com</a> |
  <script language="JavaScript"><!--
  var name = "admin";
  var domain = "muzene.com";
  document.write('<a href=\"mailto:' + name + '@' + domain + '\">');
  document.write('Contact Us</a>');
  // --></script>
</p>
<p align="center"><a href="http://giantbomb.com/"><img src="image/whiskey-powered-invert.png" alt="Whiskey Media" width="150" height="41" /></a></p>
</body>
</html>
