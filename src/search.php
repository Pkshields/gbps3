<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include 'script/base.php';

	//connect to database
	define('DBTest', TRUE);
	include 'script/db.php';

	//Get API2Array Script
	define('APITest', TRUE);
	include 'script/api2array.php';

	//Get search and page terms
	$search = $_GET['s'];
	$page = $_GET['p'];
	$searchname = str_replace(" ","+",$search);


	//Check if search is empty
	if (empty($searchname)) {
		//Put in this BS until something better
		$searchname = "Search term empty, please try again.";
	}

	//if page no is set, or else not
	if (!empty($page)) {
		//create correct offset
		$offset = $page;
		$offset--;
		$offset = $offset * 12;
		$gb = "http://www.giantbomb.com/api/search/?api_key=71ae30f6cdeb1c8d58aee42f9d0647cafefdabca &resources=video&query=" . strtolower($searchname) . "&offset=" . $offset . "&limit=12&format=json";
	}
	else {
		$gb = "http://www.giantbomb.com/api/search/?api_key=71ae30f6cdeb1c8d58aee42f9d0647cafefdabca &resources=video&query=" . strtolower($searchname) . "&limit=12&format=json";
	}
	print("test");
	print($gb);
	print(api2string($gb));
	//Create a page for bottom is necessary
	if (empty($page)) {
		$page = "1";
	}

	//Get search list
	$data = api2array($gb);
	$itemaddition = 0;

	//Cookie check
	if ($_GET["q"] == "hq" || $_GET["q"] == "lq") {
		setcookie("gb_quality", $_GET["q"], time()+60*60*24*365, "/");
	}

	//Cookie part 2 - video player part
	if (isset($_COOKIE["gb_player"])) {
		$player = $_COOKIE["gb_player"];
	}
	else {
		$player = "player";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="GBPS3: Watch <?PHP echo $search; ?> videos right from your PS3!" />
<meta name="KeyWords" content="gbps3, gb, ps3, giantbomb, giant, bomb, <?PHP echo $search; ?>" />
<link rel="icon" type="image/x-icon" href="image/favicon.ico" />
<link href="files/style.css" rel="stylesheet" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/gbps3" />
<title>GBPS3: Search - <?PHP echo $search; ?></title>
</head>

<body>

<?php include_once("script/analytics.php") ?>

<table id="table" width="1080" border="0" cellspacing="0">
  <tr>
    <td class="titleborder"><table width="1208" border="0">
      <tr>
        <td width="258"><a href="index"><img src="image/logo.gif" width="233" height="85" alt="GBPS3 Logo" /></a></td>
        <td width="622" align="center">Search - <?PHP echo $search; ?></td>
        <td width="314" class="titleright"><form id="frmSearch" name="frmSearch" method="get" action="redirect.php">
          <p>
            <input class="titletext"type="text" name="search" id="search" />
            <br />
<input type="submit" name="Submit" id="Submit" value="Search" />
          </p>
</form></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="middleborderbutton">
      <a href="index">
        <div class="button">
          <div class="buttontext">Home</div>
        </div>
      </a>
      <a href="categories">
        <div class="button">
          <div class="buttontext">Categories</div>
        </div>
      </a>
      <!-- <a href="test.php"> -->
        <div class="button">
          <div class="buttontext">&nbsp </div>
        </div>
      <!-- </a> -->
	  <?PHP
		if ($page !== "1") {
	  ?>
      <a href="search/<?PHP echo $searchname . "/" . ($page - 1); ?>">
        <div class="button">
          <div class="buttontext">Previous</div>
        </div>
      </a>
	  <?PHP
		}
		else {
	  ?>
        <div class="button">
          <div class="buttontext">&nbsp </div>
        </div>
	  <?PHP
		}

		if (($itemaddition + 12) < $data['number_of_total_results'] - $offset) {
	  ?>
      <a href="search/<?PHP echo $searchname . "/" . ($page + 1); ?>">
        <div class="button">
          <div class="buttontext">Next</div>
        </div>
      </a>
	  <?PHP
		}
		else {
	  ?>
        <div class="button">
          <div class="buttontext">&nbsp </div>
        </div>
	  <?PHP
		}
	  ?>
    </td>
  </tr>
  <tr>
    <td class="middleborder">
	  <?PHP
		for ($i = 0; $i < 12; $i++) {
			if ($itemaddition < $data['number_of_page_results'] - $offset) {
				//Check if it is a members video or not
				$sql = "SELECT Member FROM gb_video WHERE ID = '" . $data['results'][$itemaddition]['id'] . "'";
				$result = mysql_query($sql);
				$memcheck = mysql_result($result,0,"Member");
				$membertext = "";

				if ($memcheck == 1) {
					$membertext = " [MEMBER]";
				}
	  ?>
      <a href="<?PHP echo $player . "/" . $data['results'][$itemaddition]['id']; ?>">
        <div class="imgcate">
		  <img src="<?PHP echo $data['results'][$itemaddition]['image']['super_url']; ?>" class="imgcateimg" />
          <div class="imgcatetext"><?PHP echo $data['results'][$itemaddition]['name'] . $membertext; ?></div>
        </div>
      </a>
	  <?PHP
			}
			else {
				$i = 13;
			}
			$itemaddition++;
		}
	  ?>
    </td>
  </tr>
  <tr>
    <td class="bottomborder">
      <div class="floatleft">Tip:
	  <?PHP
		//Get a random tip, so random number
		//First, database
		$sql = "SELECT * FROM gb_tip";
		$result = mysql_query($sql);
		$num = mysql_num_rows($result);

		//Reduce from 1 because of computers (3 = 0 to 2)
		$num--;
		$num = rand(0, $num);

		//Get!
		echo stripslashes(mysql_result($result,$num,"Tip"));

		//Close database again
		mysql_close();
	  ?>
	  </div>
	  <?PHP
		if ($_GET["q"] == "hq" || ($_COOKIE["gb_quality"] == "hq" && $_GET["q"] !== "lq")) {
			echo '<div class="floatright">Quality: High (<a href="search/' . $search . '/' . $page . '/lq">Low?</a>)</div>';
		}
		else {
			echo '<div class="floatright">Quality: Low (<a href="search/' . $search . '/' . $page . '/hq">High?</a>)</div>';
		}
		?>
    </td>
  </tr>
</table>
<p align="center"><a href="http://muzene.com/">Muzene.com</a> |
  <script language="JavaScript"><!--
  var name = "admin";
  var domain = "muzene.com";
  document.write('<a href=\"mailto:' + name + '@' + domain + '\">');
  document.write('Contact Us</a>');
  // --></script>
</p>
<p align="center"><a href="http://giantbomb.com/"><img src="image/whiskey-powered-invert.png" alt="Whiskey Media" width="150" height="41" /></a></p>
</body>
</html>
