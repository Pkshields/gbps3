<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include 'script/base.php';
	
	//connect to database
	define('DBTest', TRUE);
	include 'script/db.php';
	
	//Get Category Name for the category we need to select random videos for
	$cat = mysql_real_escape_string($_GET['cat']);
	
	//All Videos?
	if (empty($cat) || $cat == "allvid") {
		//Get all videos from database
		$sql = "SELECT * FROM gb_video ORDER BY ID DESC";
		$result = mysql_query($sql);
		$name = "All Videos";
	}
	else {
		//get category info from database
		$sql = "SELECT * FROM gb_category WHERE ID = '" . $cat . "'";
		$result = mysql_query($sql);
		$name = mysql_result($result,0,"Name");
		$sort = mysql_result($result,0,"Sort");
		
		//Build database SQL
		$sql = "SELECT * FROM gb_video WHERE Title LIKE '" . mysql_result($result,0,"Code1") . "' ";
		if (!is_null(mysql_result($result,0,"Code2"))) {
			$sql = $sql . "OR Title LIKE '" . mysql_result($result,0,"Code2") . "' ";
		}
		$sql = $sql . "ORDER BY ID " . $sort;
		$result = mysql_query($sql);
	}
	
	//Cookie check
	if ($_GET["q"] == "hq" || $_GET["q"] == "lq") {
		setcookie("gb_quality", $_GET["q"], time()+60*60*24*365, "/");
	}
	
	//Cookie part 2 - video player part
	if (isset($_COOKIE["gb_player"])) {
		$player = $_COOKIE["gb_player"];
	}
	else {
		$player = "player";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="GBPS3: Watch <?PHP echo $name; ?> videos right from your PS3!" />
<meta name="KeyWords" content="gbps3, gb, ps3, giantbomb, giant, bomb, <?PHP echo $name; ?>" />
<link rel="icon" type="image/x-icon" href="image/favicon.ico" />
<link href="files/style.css" rel="stylesheet" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/gbps3" />
<title>GBPS3: Random!</title>
</head>

<body>

<?php include_once("script/analytics.php") ?>

<table id="table" width="1080" border="0" cellspacing="0">
  <tr>
    <td class="titleborder"><table width="1208" border="0">
      <tr>
        <td width="258"><a href="index"><img src="image/logo.gif" width="233" height="85" alt="GBPS3 Logo" /></a></td>
        <td width="622" align="center">Random!</td>
        <td width="314" class="titleright"><form id="frmSearch" name="frmSearch" method="get" action="redirect.php">
          <p>
            <input class="titletext"type="text" name="search" id="search" />
            <br />
<input type="submit" name="Submit" id="Submit" value="Search" />
          </p>
</form></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="middleborderbutton">
      <a href="index">
        <div class="button">
          <div class="buttontext">Home</div>
        </div>
      </a>
      <!-- <a href="test.php"> -->
        <div class="button">
          <div class="buttontext">&nbsp </div>
        </div>
      <!-- </a> -->
      <a href="categories">
        <div class="button">
          <div class="buttontext">Categories</div>
        </div>
      </a>
      <!-- <a href="test.php"> -->
        <div class="button">
          <div class="buttontext">&nbsp </div>
        </div>
      <!-- </a> -->
      <a href="random<?PHP if (!empty($cat)) { echo "/" . $cat; } ?>">
        <div class="button">
          <div class="buttontext">Randomise</div>
        </div>
      </a>
    </td>
  </tr>
  <tr>
    <td class="middleborder">
	  <?PHP
		for ($i = 0; $i < 12; $i++) {
			//Get a random number between 0 and total database
			$totalvids = mysql_num_rows($result);
			$totalvids--;
			$randvid = rand(0, $totalvids);
			
			//Add members text if necessary
			if (mysql_result($result,$randvid,"Member") == 1) {
				$membertext = " [MEMBER]";
			}
			else {
				$membertext = "";
			}
	  ?>
      <a href="<?PHP echo $player . "/" . mysql_result($result,$randvid,"ID"); ?>">
        <div class="imgcate">
		  <img src="http://static.giantbomb.com/uploads/scale_large/<?PHP echo mysql_result($result,$randvid,"Image"); ?>" class="imgcateimg" />
          <div class="imgcatetext"><?PHP echo mysql_result($result,$randvid,"Title") . $membertext; ?></div>
        </div>
      </a>
	  <?PHP
		}
	  ?>
    </td>
  </tr>
  <tr>
    <td class="bottomborder">
      <div class="floatleft">Tip: 
	  <?PHP
		//Get a random tip, so random number
		//First, database
		$sql = "SELECT * FROM gb_tip";
		$result = mysql_query($sql);
		$num = mysql_num_rows($result);
		
		//Reduce from 1 because of computers (3 = 0 to 2)
		$num--;
		$num = rand(0, $num);
		
		//Get!
		echo stripslashes(mysql_result($result,$num,"Tip"));
		
		//Close database again
		mysql_close();
	  ?>
	  </div>
	  <?PHP
		if ($_GET["q"] == "hq" || ($_COOKIE["gb_quality"] == "hq" && $_GET["q"] !== "lq")) {
			echo '<div class="floatright">Quality: High (<a href="random/' . $cat . '/lq">Low?</a>)</div>';
		}
		else {
			echo '<div class="floatright">Quality: Low (<a href="random/' . $cat . '/hq">High?</a>)</div>';
		}
		?>
    </td>
  </tr>
</table>
<p align="center"><a href="http://muzene.com/">Muzene.com</a> | 
  <script language="JavaScript"><!--
  var name = "admin";
  var domain = "muzene.com";
  document.write('<a href=\"mailto:' + name + '@' + domain + '\">');
  document.write('Contact Us</a>');
  // --></script>
</p>
<p align="center"><a href="http://giantbomb.com/"><img src="image/whiskey-powered-invert.png" alt="Whiskey Media" width="150" height="41" /></a></p>
</body>
</html>