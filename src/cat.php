<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include 'script/base.php';
	
	//connect to database
	define('DBTest', TRUE);
	include 'script/db.php';
	
	//Get Category Name
	$cat = mysql_real_escape_string($_GET['cat']);
	
	//All Videos?
	if ($cat == "allvid") {
		//get all video info from database
		$sql = "SELECT * FROM gb_video ORDER BY ID DESC";
		$result = mysql_query($sql);
		$name = "All Videos";
	}
	else {
		//get category info from database
		$sql = "SELECT * FROM gb_category WHERE ID = '" . $cat . "'";
		$result = mysql_query($sql);
		$name = stripslashes(mysql_result($result,0,"Name"));
		$sort = mysql_result($result,0,"Sort");
		
		//Build database SQL
		$sql = "SELECT * FROM gb_video WHERE Title LIKE '" . mysql_result($result,0,"Code1") . "' ";
		if (!is_null(mysql_result($result,0,"Code2"))) {
			$sql = $sql . "OR Title LIKE '" . mysql_result($result,0,"Code2") . "' ";
		}
		$sql = $sql . "ORDER BY ID " . $sort;
		$result = mysql_query($sql);
	}
	
	//Page calculations
	$page = mysql_real_escape_string($_GET['p']);
	$totalvids = mysql_num_rows($result);
	if (empty($page)) {
		$itemaddition = 0;
		$page = "1";
	}
	else {
		$itemaddition = ($page - 1) * 12;
	}
	$totalpage = ceil($totalvids/12);
	
	//Cookie check
	if ($_GET["q"] == "hq" || $_GET["q"] == "lq") {
		setcookie("gb_quality", $_GET["q"], time()+60*60*24*365, "/");
	}
	
	//Cookie part 2 - video player part
	if (isset($_COOKIE["gb_player"])) {
		$player = $_COOKIE["gb_player"];
	}
	else {
		$player = "player";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="GBPS3: Watch <?PHP echo $name; ?> videos right from your PS3!" />
<meta name="KeyWords" content="gbps3, gb, ps3, giantbomb, giant, bomb, <?PHP echo $name; ?>" />
<link rel="icon" type="image/x-icon" href="image/favicon.ico" />
<link href="files/style.css" rel="stylesheet" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/gbps3" />
<title>GBPS3: <?PHP echo $name; ?></title>
</head>

<body>

<?php include_once("script/analytics.php") ?>

<table id="table" width="1080" border="0" cellspacing="0">
  <tr>
    <td class="titleborder"><table width="1208" border="0">
      <tr>
        <td width="258"><a href="index"><img src="image/logo.gif" width="233" height="85" alt="GBPS3 Logo" /></a></td>
        <td width="622" align="center"><?PHP echo $name; ?></td>
        <td width="314" class="titleright"><form id="frmSearch" name="frmSearch" method="get" action="redirect.php">
          <p>
            <input class="titletext"type="text" name="search" id="search" />
            <br />
<input type="submit" name="Submit" id="Submit" value="Search" />
          </p>
</form></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="middleborderbutton">
      <a href="index">
        <div class="button">
          <div class="buttontext">Home</div>
        </div>
      </a>
      <a href="categories">
        <div class="button">
          <div class="buttontext">Categories</div>
        </div>
      </a>
      <a href="random/<?PHP echo $cat; ?>">
        <div class="button">
          <div class="buttontext">Random</div>
        </div>
      </a>
	  <?PHP
		if ($page !== "1") {
	  ?>
      <a href="cat/<?PHP echo $cat . "/" . ($page - 1); ?>">
        <div class="button">
          <div class="buttontext">Previous</div>
        </div>
      </a>
	  <?PHP
		}
		else {
	  ?>
        <div class="button">
          <div class="buttontext">&nbsp </div>
        </div>
	  <?PHP
		}
		
		if (($itemaddition + 12) < $totalvids) {
	  ?>
      <a href="cat/<?PHP echo $cat . "/" . ($page + 1); ?>">
        <div class="button">
          <div class="buttontext">Next</div>
        </div>
      </a>
	  <?PHP
		}
		else {
	  ?>
        <div class="button">
          <div class="buttontext">&nbsp </div>
        </div>
	  <?PHP
		}
	  ?>
    </td>
  </tr>
  <tr>
    <td class="middleborder">
	  <?PHP
		for ($i = 0; $i < 12; $i++) {
			if ($itemaddition < $totalvids) {
				//Check if it is a members video or not
				$sql2 = "SELECT Member FROM gb_video WHERE ID = '" . mysql_result($result,$itemaddition,"ID") . "'";
				$result2 = mysql_query($sql2);
				$memcheck = mysql_result($result2,0,"Member");
				$membertext = "";
				
				if ($memcheck == 1) {
					$membertext = " [MEMBER]";
				}
	  ?>
      <a href="<?PHP echo $player . "/" . mysql_result($result,$itemaddition,"ID"); ?>">
        <div class="imgcate">
		  <img src="http://static.giantbomb.com/uploads/scale_large/<?PHP echo mysql_result($result,$itemaddition,"Image"); ?>" class="imgcateimg" />
          <div class="imgcatetext"><?PHP echo mysql_result($result,$itemaddition,"Title") . $membertext; ?></div>
        </div>
      </a>
	  <?PHP
			}
			else {
				$i = 13;
			}
			$itemaddition++;
		}
	  ?>
    </td>
  </tr>
  <tr>
    <td class="bottomborder">
      <div class="floatleft">Tip: 
	  <?PHP
		//Get a random tip, so random number
		//First, database
		$sql = "SELECT * FROM gb_tip";
		$result = mysql_query($sql);
		$num = mysql_num_rows($result);
		
		//Reduce from 1 because of computers (3 = 0 to 2)
		$num--;
		$num = rand(0, $num);
		
		//Get!
		echo stripslashes(mysql_result($result,$num,"Tip"));
		
		//Close database again
		mysql_close();
	  ?>
	  </div>
	  <?PHP
		if ($_GET["q"] == "hq" || ($_COOKIE["gb_quality"] == "hq" && $_GET["q"] !== "lq")) {
			echo '<div class="floatright">Quality: High (<a href="cat/' . $cat . '/' . $page . '/lq">Low?</a>)</div>';
		}
		else {
			echo '<div class="floatright">Quality: Low (<a href="cat/' . $cat . '/' . $page . '/hq">High?</a>)</div>';
		}
		?>
    </td>
  </tr>
</table>
<p align="center"><a href="http://muzene.com/">Muzene.com</a> | 
  <script language="JavaScript"><!--
  var name = "admin";
  var domain = "muzene.com";
  document.write('<a href=\"mailto:' + name + '@' + domain + '\">');
  document.write('Contact Us</a>');
  // --></script>
</p>
<p align="center"><a href="http://giantbomb.com/"><img src="image/whiskey-powered-invert.png" alt="Whiskey Media" width="150" height="41" /></a></p>
</body>
</html>