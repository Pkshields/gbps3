<?PHP
	//Get search Term
	$search = addslashes($_GET['search']);
	
	//Part 1 - Check if it is Giant Bomb link
	$pos = strtolower($search);
	$pos = strpos($search,'giantbomb.com');

	if($pos !== false) {
		$link = "player.php?v=" . $search;
		header ("location: " . $link);
		die();
	}

	//Part 2 - if not Giant Bomb link, redirect to search
	$search =  preg_replace("/[^a-zA-Z0-9\s]/", "", $search); //Remove special characters, see if it fixes search
	$search = str_replace(" ", "+", $search);
	$link = "search/" . $search;
	header ("location: " . $link);
?>