<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include 'script/base.php';
	
	//Login check - always before database connection!
	define('LoginTest', TRUE);
	include 'script/logincheck.php';
	$member = logincheck();
	
	//connect to database
	define('DBTest', TRUE);
	include 'script/db.php';
	
	//Cookie check
	if ($_GET["q"] == "hq" || $_GET["q"] == "lq") {
		setcookie("gb_quality", $_GET["q"], time()+60*60*24*365, "/");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="GBPS3: Watch GiantBomb.com videos right from your PS3!" />
<meta name="KeyWords" content="gbps3, gb, ps3, giantbomb, giant, bomb" />
<link rel="icon" type="image/x-icon" href="image/favicon.ico" />
<link href="files/style.css" rel="stylesheet" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/gbps3" />
<title>GBPS3 - Members Login</title>
</head>

<body>

<?php include_once("script/analytics.php") ?>

<table id="table" border="0" cellspacing="0">
  <tr>
    <td class="titleborder"><table width="1208" border="0">
      <tr>
        <td width="258"><a href="index"><img src="image/logo.gif" width="233" height="85" alt="GBPS3 Logo" /></a></td>
        <td width="622" align="center">&nbsp;</td>
        <td width="314" class="titleright"><form id="frmSearch" name="frmSearch" method="get" action="redirect.php">
          <p>
            <input class="titletext" type="text" name="search" id="search" />
            <br />
<input type="submit" name="Submit" id="Submit" value="Search" />
          </p>
</form></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="middleborderbutton">
      <a href="index">
        <div class="button">
          <div class="buttontext">Home</div>
        </div>
      </a>
      <a href="cat/allvid">
        <div class="button">
          <div class="buttontext">All Videos</div>
        </div>
      </a>
      <a href="cat/ql">
        <div class="button">
          <div class="buttontext">Quick Look</div>
        </div>
      </a>
      <a href="cat/review">
        <div class="button">
          <div class="buttontext">Review</div>
        </div>
      </a>
      <a href="categories">
        <div class="button">
          <div class="buttontext">Categories</div>
        </div>
      </a>
    </td>
  </tr>
  <tr>
    <td class="middleborder">
	<h3>Members Login!</h3>
	<?PHP
		if (isset($_GET["error"])) {
	?>
	<h4>Error: Required field(s) "<?PHP echo $_GET["error"]; ?>" are missing or incorrect, please try again.</h4>
	<?PHP
		}
	?>
	
	<?PHP
		//Start of the different page types
		//If logged in and user wants to change password, show that
		if ($member) {
			//Quick! Get the username!
			$sql = "SELECT Username FROM gb_user WHERE ID = " . substr($_COOKIE["gb_login"], -1, 1);
			$result = mysql_query($sql);
			$username = mysql_result($result,0,"Username");
	?>
	
	<?PHP
		}
		//If logged in, show the logout page
		if ($member && isset($_POST["cp"])) {
	?>
	<form id="frmUser" name="frmUser" method="post" action="script/userlogin.php">
	  <h4>Change Password:</h4>
      <ul>
	    <li>Password: <input class="loginbox" name="txtPassword" type="password" id="txtPassword" maxlength="20" /></li>
	    <li>Repeat Password: <input class="loginbox" name="txtRepeatPassword" type="password" id="txtRepeatPassword" maxlength="20" /></li>
		<input type="submit" name="SubmitPass" value="Change Password" />
      </ul>
    </form>
	<?PHP
		}
		//Else, show the logout page
		else {
	?>
	<form id="frmUser" name="frmUser" method="post" action="script/userlogin.php">
      <ul>
	    <li>Username: <input class="loginbox" name="txtUsername" type="text" id="txtUsername" maxlength="20" /></li>
	    <li>Password: &nbsp;<input class="loginbox" name="txtPassword" type="password" id="txtPassword" maxlength="20" /></li>
      </ul>
	  <input type="submit" name="SubmitIn" value="Login" />
    </form>
	<p>Note: Member login is in beta right now, so no registration is available, as well as account editing etc. Come back to this page to log out again.</p>
	<?PHP
		}
	?>
    </td>
  </tr>
  <tr>
    <td class="bottomborder">
      <div class="floatleft">Tip: 
	  <?PHP
		//Get a random tip, so random number
		//First, database
		$sql = "SELECT * FROM gb_tip";
		$result = mysql_query($sql);
		$num = mysql_num_rows($result);
		
		//Reduce from 1 because of computers (3 = 0 to 2)
		$num--;
		$num = rand(0, $num);
		
		//Get!
		echo stripslashes(mysql_result($result,$num,"Tip"));
		
		//Close database again
		mysql_close();
	  ?>
	  </div>
	  <?PHP
		if ($_GET["q"] == "hq" || ($_COOKIE["gb_quality"] == "hq" && $_GET["q"] !== "lq")) {
			echo '<div class="floatright">Quality: High (<a href="index/lq">Low?</a>)</div>';
		}
		else {
			echo '<div class="floatright">Quality: Low (<a href="index/hq">High?</a>)</div>';
		}
		?>
    </td>
  </tr>
</table>
<p align="center"><a href="http://muzene.com/">Muzene.com</a> | 
  <script language="JavaScript"><!--
  var name = "admin";
  var domain = "muzene.com";
  document.write('<a href=\"mailto:' + name + '@' + domain + '\">');
  document.write('Contact Us</a>');
  // --></script>
</p>
<p align="center"><a href="http://giantbomb.com/"><img src="image/whiskey-powered-invert.png" alt="Whiskey Media" width="150" height="41" /></a></p>
</body>
</html>