<?PHP
error_reporting(E_ALL);
ini_set("display_errors", 1);

	//Quote from CMSE: "Includes suck in PHP apparently. Fix."
	$dirBeforeCurrentDir = dirname(dirname(__FILE__));

	//connect to database
	define('DBTest', TRUE);
	include $dirBeforeCurrentDir . '/script/db.php';

	//Get API2Array Script
	define('APITest', TRUE);
	include $dirBeforeCurrentDir . '/script/api2array.php';

	//Get totalvid number from database
	$sql = "SELECT TotalVid FROM gb_totalvid WHERE ID = '1'";
	$result = mysql_query($sql);
	$totalvid = mysql_result($result,0,"TotalVid");
	$totalvid = $totalvid - 10;

	//Get the total number of subscriber videos we have found
	$sql = "SELECT TotalVid FROM gb_totalvid WHERE ID = '2'";
	$result = mysql_query($sql);
	$totalsubvid = mysql_result($result,0,"TotalVid");

	//Get totalvid number from GB
	$gb = "http://www.giantbomb.com/api/videos/?api_key=71ae30f6cdeb1c8d58aee42f9d0647cafefdabca &sort=publish_date&limit=1&field_list=number_of_total_results&format=json";
	$data = api2array($gb);
	$newtotalvid = $data['number_of_total_results'];

	//If different, then update
	if ($newtotalvid > $totalvid)
	{
		//Start loop from one before last video, just to make sure
		$totalvid--;

		//Create a loop to continue until update is done
		for ($i=$totalvid; $i<$newtotalvid; $i=$i2)
		{
			//Open API with TotalVid offset + XML2Array
			$gb = "http://www.giantbomb.com/api/videos/?api_key=71ae30f6cdeb1c8d58aee42f9d0647cafefdabca &sort=publish_date&limit=10&field_list=name,id,image,publish_date&offset=" . $i . "&format=json";
			$data = api2array($gb);
			$limit = $data['number_of_page_results'];
			$offset = $limit;

			//Get the next 10 subscriber videos
			//Used to check if any of the new videos found are sub only
			$gb = "http://www.giantbomb.com/api/videos/?api_key=71ae30f6cdeb1c8d58aee42f9d0647cafefdabca &sort=publish_date&limit=10&field_list=id&offset=" . $totalsubvid . "&video_type=10&format=json";
			$subdata = api2array($gb);

			//Check every array
			for ($j=0; $j<$limit; $j++)
			{
				//Check Array if fully formed
				$name = $data['results'][$j]['name'];
				$id = $data['results'][$j]['id'];
				$superimage = substr ($data['results'][$j]['image']['super_url'], 35+13);
				$date = $data['results'][$j]['publish_date'];

				//Check if the new video is a known sub video or not
				$member = 0;
				foreach ($subdata['results'] as &$subinfo) {
					if ($id == $subinfo['id']) {
						//If yes, set to sub, increment the number of sub videos and break outta loop
						$member = 1;
						$totalsubvid++;
						break;
					}
				}

				if (!empty($name) && !empty($id) && !empty($superimage) && !empty($date))
				{
					//Add game to database
					$name = addslashes($name);
					$sql = "INSERT INTO gb_video(Title,ID,Image,Date,Member) VALUES ('" . $name . "','" . $id . "','" . $superimage . "','" . $date . "','" . $member . "')";
					$result=mysql_query($sql);
				}
				else
				{
					$offset--;
				}
			}

			//Set new offset value
			$i2 = $i + $offset;

			//Add new TotalVid to database
			$sql = "UPDATE gb_totalvid SET TotalVid ='". $i2 . "' WHERE ID ='1'";
			$result=mysql_query($sql);
			$sql = "UPDATE gb_totalvid SET TotalVid ='". $totalsubvid . "' WHERE ID ='2'";
			$result=mysql_query($sql);
		}

		//TEMP - Add again, just to make sure
		$sql = "UPDATE gb_totalvid SET TotalVid ='". $newtotalvid . "' WHERE ID ='1'";
		$result=mysql_query($sql);
	}

	//Done!
	mysql_close();

	//If from QL list, redirect
	if (!empty($_SESSION['Redirect'])) {
		header ("location: ../ql.php");
		unset($_SESSION['Redirect']);
		die();
	}

	//If not, end
	die("Done! All relevent videos are now in database");
?>
