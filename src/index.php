<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include 'script/base.php';
	
	//Login check - always before database connection!
	define('LoginTest', TRUE);
	include 'script/logincheck.php';
	$member = logincheck();
	
	//connect to database
	define('DBTest', TRUE);
	include 'script/db.php';
	
	//Get data from gb_feature
	$sql = "SELECT * FROM gb_feature ORDER BY ID ASC";
	$result = mysql_query($sql);
	
	//Store category names and num in array
	$catid = array();
	$catnum = array();
	for ($i = 0; $i < 6; $i++) {
		$catid[$i] = addslashes(mysql_result($result,$i,"Category"));
		$catnum[$i] = addslashes(mysql_result($result,$i,"Num"));
	}
	
	//Get data from gb_category + Store category search terms
	$catcode1 = array();
	$catcode2 = array();
	for ($i = 0; $i < 6; $i++) {
		$sql = "SELECT * FROM gb_category WHERE ID = '" . $catid[$i] . "'";
		$result = mysql_query($sql);
		
		//Check if it is real or an override
		if (mysql_num_rows($result) == 0) {
			$catcode1[$i] = $catid[$i];
			$catcode2[$i] = NULL;
		}
		else {
			$catcode1[$i] = mysql_result($result,0,"Code1");
			$catcode2[$i] = mysql_result($result,0,"Code2");
		}
	}
	
	//Cookie check
	if ($_GET["q"] == "hq" || $_GET["q"] == "lq") {
		setcookie("gb_quality", $_GET["q"], time()+60*60*24*365, "/");
	}
	
	//Cookie part 2 - video player part
	if (isset($_COOKIE["gb_player"])) {
		$player = $_COOKIE["gb_player"];
	}
	else {
		$player = "player";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="GBPS3: Watch GiantBomb.com videos right from your PS3!" />
<meta name="KeyWords" content="gbps3, gb, ps3, giantbomb, giant, bomb" />
<link rel="icon" type="image/x-icon" href="image/favicon.ico" />
<link href="files/style.css" rel="stylesheet" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/gbps3" />
<title>GBPS3 - Watch GiantBomb.com Videos on Your PS3!</title>
</head>

<body>

<?php include_once("script/analytics.php") ?>

<table id="table" border="0" cellspacing="0">
  <tr>
    <td class="titleborder"><table width="1208" border="0">
      <tr>
        <td width="258"><a href="index"><img src="image/logo.gif" width="233" height="85" alt="GBPS3 Logo" /></a></td>
        <td width="622" align="center">
		<?PHP
			if ($member) { echo '<a href="login">Welcome Member!</a>'; }
			else { echo "&nbsp"; }
		?>
		</td>
        <td width="314" class="titleright"><form id="frmSearch" name="frmSearch" method="get" action="redirect.php">
          <p>
            <input class="titletext" type="text" name="search" id="search" />
            <br />
<input type="submit" name="Submit" id="Submit" value="Search" />
          </p>
</form></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="middleborderbutton">
      <a href="index">
        <div class="button">
          <div class="buttontext">Home</div>
        </div>
      </a>
      <a href="cat/allvid">
        <div class="button">
          <div class="buttontext">All Videos</div>
        </div>
      </a>
      <a href="cat/ql">
        <div class="button">
          <div class="buttontext">Quick Look</div>
        </div>
      </a>
      <a href="cat/review">
        <div class="button">
          <div class="buttontext">Review</div>
        </div>
      </a>
      <a href="categories">
        <div class="button">
          <div class="buttontext">Categories</div>
        </div>
      </a>
    </td>
  </tr>
  <tr>
    <td class="middleborder">
	<?PHP
		for ($i = 0; $i < 6; $i++) {
			//get correct video details from database
			$sql = "SELECT * FROM gb_video WHERE Title LIKE '" . $catcode1[$i] . "' ";
			if (!is_null($catcode2[$i])) {
				$sql = $sql . "OR Title LIKE '" . $catcode2[$i] . "' ";
			}
			$sql = $sql . "ORDER BY ID DESC";
			$result = mysql_query($sql);
			
			//Check if video is a members video
			$membertext = "";
			$memcheck = mysql_result($result,$catnum[$i],"Member");
			if ($memcheck == 1) {
				$membertext = " [MEMBER]";
			}
	?>
      <a href="<?PHP echo $player . "/" . mysql_result($result,$catnum[$i],"ID"); ?>">
        <div class="feat">
		  <img src="http://static.giantbomb.com/uploads/scale_large/<?PHP echo mysql_result($result,$catnum[$i],"Image"); ?>" class="featimg" />
          <div class="feattext"><?PHP echo mysql_result($result,$catnum[$i],"Title") . $membertext; ?></div>
        </div>
      </a>
	<?PHP
		}
	?>
    </td>
  </tr>
  <tr>
    <td class="bottomborder">
      <div class="floatleft">Tip: 
	  <?PHP
		//Get a random tip, so random number
		//First, database
		$sql = "SELECT * FROM gb_tip";
		$result = mysql_query($sql);
		$num = mysql_num_rows($result);
		
		//Reduce from 1 because of computers (3 = 0 to 2)
		$num--;
		$num = 0; //rand(0, $num);
		
		//Get!
		echo stripslashes(mysql_result($result,$num,"Tip"));
		
		//Close database again
		mysql_close();
	  ?>
	  </div>
	  <?PHP
		if ($_GET["q"] == "hq" || ($_COOKIE["gb_quality"] == "hq" && $_GET["q"] !== "lq")) {
			echo '<div class="floatright">Quality: High (<a href="index/lq">Low?</a>)</div>';
		}
		else {
			echo '<div class="floatright">Quality: Low (<a href="index/hq">High?</a>)</div>';
		}
		?>
    </td>
  </tr>
</table>
<p align="center"><a href="http://muzene.com/">Muzene.com</a> | 
  <script language="JavaScript"><!--
  var name = "admin";
  var domain = "muzene.com";
  document.write('<a href=\"mailto:' + name + '@' + domain + '\">');
  document.write('Contact Us</a>');
  // --></script>
</p>
<p align="center"><a href="http://giantbomb.com/"><img src="image/whiskey-powered-invert.png" alt="Whiskey Media" width="150" height="41" /></a></p>
</body>
</html>