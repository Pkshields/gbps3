<?PHP
	//Base URL
	define('BaseTest', TRUE);
	include 'script/base.php';
	
	//connect to database
	define('DBTest', TRUE);
	include 'script/db.php';
	
	//Cookie check
	if ($_GET["q"] == "hq" || $_GET["q"] == "lq") {
		setcookie("gb_quality", $_GET["q"], time()+60*60*24*365, "/");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?PHP baseurl(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="GBPS3: Watch GiantBomb.com videos right from your PS3!" />
<meta name="KeyWords" content="gbps3, gb, ps3, giantbomb, giant, bomb" />
<link rel="icon" type="image/x-icon" href="image/favicon.ico" />
<link href="files/style.css" rel="stylesheet" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/gbps3" />
<title>GBPS3 - Error <?PHP echo $_GET['error']; ?></title>
</head>

<body>

<?php include_once("script/analytics.php") ?>

<table id="table" border="0" cellspacing="0">
  <tr>
    <td class="titleborder"><table width="1208" border="0">
      <tr>
        <td width="258"><a href="index"><img src="image/logo.gif" width="233" height="85" alt="GBPS3 Logo" /></a></td>
        <td width="622" align="center">&nbsp;</td>
        <td width="314" class="titleright"><form id="frmSearch" name="frmSearch" method="get" action="redirect.php">
          <p>
            <input class="titletext" type="text" name="search" id="search" />
            <br />
<input type="submit" name="Submit" id="Submit" value="Search" />
          </p>
</form></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="middleborderbutton">
      <a href="index">
        <div class="button">
          <div class="buttontext">Home</div>
        </div>
      </a>
      <a href="cat/allvid">
        <div class="button">
          <div class="buttontext">All Videos</div>
        </div>
      </a>
      <a href="cat/ql">
        <div class="button">
          <div class="buttontext">Quick Look</div>
        </div>
      </a>
      <a href="cat/review">
        <div class="button">
          <div class="buttontext">Review</div>
        </div>
      </a>
      <a href="categories">
        <div class="button">
          <div class="buttontext">Categories</div>
        </div>
      </a>
    </td>
  </tr>
  <tr>
    <td class="middleborder">
	<?PHP
		if ($_GET['error'] == "2301") {
			echo '<p id="error">Was the link you entered a GiantBomb.com link? [Error Code: 2301]</p>';
		}
		if ($_GET['error'] == "2302") {
			echo '<p id="error">API Error: Video not found. Was the link/ID number correct? If you clicked on a link from the categories page, please <a href="contactus" >drop us a quick note here.</a> [Error Code: 2302]</p>';
		}
		if ($_GET['error'] == "2303") {
			echo '<p id="error">Oh, sorry, server just pulled a Brad. Try refreshing or, if it still isn\'t working, the search bar will still work. [Error Code: 2303]</p>';
		}
		if ($_GET['error'] == "404") {
			echo '<p id="error">404: File not Found. Did you follow a link on the site? Please drop us a quick note at the bottom if you did.</p>';
		}
	?>
    </td>
  </tr>
  <tr>
    <td class="bottomborder">
      <div class="floatleft">Tip: 
	  <?PHP
		//Get a random tip, so random number
		//First, database
		$sql = "SELECT * FROM gb_tip";
		$result = mysql_query($sql);
		$num = mysql_num_rows($result);
		
		//Reduce from 1 because of computers (3 = 0 to 2)
		$num--;
		$num = rand(0, $num);
		
		//Get!
		echo stripslashes(mysql_result($result,$num,"Tip"));
		
		//Close database again
		mysql_close();
	  ?>
	  </div>
	  <?PHP
		if ($_GET["q"] == "hq" || ($_COOKIE["gb_quality"] == "hq" && $_GET["q"] !== "lq")) {
			echo '<div class="floatright">Quality: High (<a href="index/lq">Low?</a>)</div>';
		}
		else {
			echo '<div class="floatright">Quality: Low (<a href="index/hq">High?</a>)</div>';
		}
		?>
    </td>
  </tr>
</table>
<p align="center"><a href="http://muzene.com/">Muzene.com</a> | 
  <script language="JavaScript"><!--
  var name = "admin";
  var domain = "muzene.com";
  document.write('<a href=\"mailto:' + name + '@' + domain + '\">');
  document.write('Contact Us</a>');
  // --></script>
</p>
<p align="center"><a href="http://giantbomb.com/"><img srcsrc="image/whiskey-powered-invert.png" alt="Whiskey Media" width="150" height="41" /></a></p>
</body>
</html>