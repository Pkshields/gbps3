# GBPS3

A Giant Bomb UI and video player, optimised for playback on the PlayStation 3.

This is a very old project, dating back to 2009 when I was 17. The code is absolutely awful, but it was my first major project and it was something I used daily for many years, so after after many years of building much better projcts professionally and personally, it still holds sentimental value to me :hugs:

## Deployment

This repo contains a modified code set from the last version that was deployed to a server which removes any personal credentials or IDs that were hardcoded into the app. A [Vagrant](https://www.vagrantup.com/) script is supplied to run the webapp locally, but this webapp **should not be run in production again**. It was built for a much older version of PHP which is no longer available and it does not utilise many of the now standard techniques used to make webapps and APIs secure. I was 17...

### Run Locally

First, install [Vagrant](https://www.vagrantup.com/docs/installation/). Then, navigate to the root of this source directory and run...

```vagrant up```

Once the VM has started successfully, the app will be available at `http://192.168.33.10/`.

### Notes

A number of hardcoded IDs have been removed from this app. A Giant Bomb API key (linked to a dummy Giant Bomb account) has been left, but IDs for Google analytics and reCAPTCHA and credentals for the servce previously used as a backup database host have been removed.
